## 快速 FIR IP 核
此设计是 40 阶通用FIR滤波器，采用直接型结构设计。

### 优点 速度快
在 Vivado 2016.2 上测试，设置约束可达 1.8 ns，即运行速度可达到最高 550M.

全部使用DSP48E1 内部资源，不占用其它资源： **使用41个DSP资源块**。

### 缺点 DSP资源占用多
由于使用的是直接型 FIR 滤波器结果，所以 占用DSP资源较多，是对称型的两倍。

### 资源占用表
使用器件：
> Kirtex 7 325t ffg900-2L 

| Resoure   |  Utilization|   Available|  Utilization % |
| :-------- | --------:| :------: |:------: |
| DSP    |   41|  840|4.88|
| IO    |   33|  500|6.60|
| BUFG    |   1|  32|3.13|

![FIR fastest 资源占用图](https://portrait.gitee.com/uploads/avatars/user/527/1583144_upsame_1578955158.png!avatar60)


