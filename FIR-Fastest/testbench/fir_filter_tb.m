%%
% https://blog.csdn.net/yanshanyan/article/details/82414688
clear;
 
file_name='fsave.txt';
fid = fopen(file_name,'r');
c = fscanf(fid,'%d');
fclose(fid);
for i=1: length(c)
    if(c(i)>32767)
        b(i) =  c(i)-65536;
    else b(i) =  c(i);
    end
end
d1=b(1:2:end);
d2=b(2:2:end);
stg=40;
wn=fir1(stg,10/51.2); % Lowpass Filter
wpara=int32(32768*wn);% Double-point to fixed-point
for i= 1: (length(d1)-stg)
    e(i)=sum(wpara.*int32(d1(i:i+stg)));    % convolution -- Chinese:juanji
end
f=int16(e/32768);
m1=d2(stg+3:end);
m2=f(1:end-2);
n=int32(m1)-int32(m2);
subplot(3,1,1);
plot(m2);
subplot(3,1,2);
plot(m1);
subplot(3,1,3);
plot(n);
 
 