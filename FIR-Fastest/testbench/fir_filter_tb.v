`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/22 18:57:26
// Design Name: 
// Module Name: fir_filter_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns/1ns;
module fir_filter_tb;
  
  parameter dat_width = 16;
  
  reg clk;
  reg [dat_width-1 :0] dat;
  
  wire[dat_width-1 :0] fil_o;
 
  integer handle1;
  reg fs;
  initial
  begin//sequence block    
  	 handle1 =$fopen("fsave.txt"); 
  	 #1080 fs = 1; 	//fir filter outputs  first data 
    #100000 fs=0;
    $fclose(handle1);
    # 1000;
    $stop;
  end 
  initial 
  fork    //synchrous block
    
    dat = 0;
    clk =0;
    forever #10 clk=~clk;  
  join
  
  integer seed;
  
  always @(posedge clk )  
  begin
    dat <= $random(seed);  //generate a random ,
    if(fs == 1)
      $fwrite(handle1,"%d  %d \n",dat,fil_o);
  end
    
    
  fir_filter fir_filter_inst(clk, dat, fil_o);
  
endmodule
 